function subnavOpener() {
    $('.header__nav-item--is-submenu').on('click', function () {
        $(this).toggleClass('active');
    })
}
function mobmenu() {
    $('.header__mobmenu').on('click', function () {
        $('.header__middle').toggleClass('opened');
        $('.header__shadow').toggleClass('active');
    });
    $('.header__shadow').on('click', function () {
        $('.header__middle').toggleClass('opened');
        $('.header__shadow').toggleClass('active');
    })
}
function footerMobNav() {
    $('.footer__nav-item').on('click', function () {
        $(this).toggleClass('active');
    })
}
function initMainSlider() {
    if($('.banner__slider')) {
        var slider = $('.banner__slider'),
            arrows = $('.banner__slider-arrow');
        slider.slick({
            infinite: true,
            arrows: false,
            dots: true
        })
        arrows.on('click', function() {
            slider.slick($(this).attr('data-slider'));
        });
    }

}
function initTariffsSlider() {
    if($('.tariffs__slider')) {
        $('.tariffs__slider').slick({
            slidesToShow: 2,
            infinite: true,
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        variableWidth: true,
                        autoplay:false
                    }
                }
            ]
        })
    }
}
function accordion() {
    $('.js-accord-head').on('click', function () {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).siblings('.js-accord-content').slideUp();
        } else {
            $('.js-accord-head').removeClass('active');
            $('.js-accord-content').slideUp();
            $(this).addClass('active');
            $(this).siblings('.js-accord-content').slideDown();
        }
    })
}
function documentsAccordion() {
    $('.js-documents-accord-head').on('click', function () {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).siblings('.js-documents-accord-content').slideUp();
        } else {
            $(this).addClass('active');
            $(this).siblings('.js-documents-accord-content').slideDown();
        }
    })
}
function mobheaderAppClose() {
    $('.header__app-close').on('click', function () {
        $('.header__app').remove();
    })
}

function servicesSliderInit() {
    if($('.services__slider')) {
        $('.services__slider').slick({
            slidesToShow: 3,
            variableWidth: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                        autoplay: false
                    }
                }
            ]
        });
    }
}

function termsSliderInit() {
    if($('.js-terms-slider')) {
        $('.js-terms-slider').slick({
            slidesToShow: 3,
            infitine: true,
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        autoplay: false
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        })
    }
}
function generalTermsSliderInit() {
    if($('.js-general-terms-slider')) {
        $('.js-general-terms-slider').slick({
            slidesToShow: 2,
            infitine: true,
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        autoplay: false
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        })
    }
}
function typesAccord() {
    if (typesFlag) {
        $('.types__box-list').slideUp();
        console.log('resize');
        $(document).on('click', '.types__box-subtitle.active', function () {
            $(this).removeClass('active');
            $(this).next('.types__box-list').slideUp();
            console.log('adctive');
        })
        $(document).on('click', '.types__box-subtitle:not(.active)', function () {
            $('.types__box-subtitle').removeClass('active');
            $('.types__box-list').slideUp();
            $(this).addClass('active');
            $(this).next('.types__box-list').slideDown();
            console.log('not-adctive');
        });
        typesFlag = false;
        triggerTypesFlag = false;
    }

}

function scrollTop() {
    $('.gototop__btn').on('click', function () {
        $('html, body').animate({scrollTop:0}, '300');
    })
}

(function($) {

    var Defaults = $.fn.select2.amd.require('select2/defaults');

    $.extend(Defaults.defaults, {
        dropdownPosition: 'auto'
    });

    var AttachBody = $.fn.select2.amd.require('select2/dropdown/attachBody');

    var _positionDropdown = AttachBody.prototype._positionDropdown;

    AttachBody.prototype._positionDropdown = function() {

        var $window = $(window);

        var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
        var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');

        var newDirection = null;

        var offset = this.$container.offset();

        offset.bottom = offset.top + this.$container.outerHeight(false);

        var container = {
            height: this.$container.outerHeight(false)
        };

        container.top = offset.top;
        container.bottom = offset.top + container.height;

        var dropdown = {
            height: this.$dropdown.outerHeight(false)
        };

        var viewport = {
            top: $window.scrollTop(),
            bottom: $window.scrollTop() + $window.height()
        };

        var enoughRoomAbove = viewport.top < (offset.top - dropdown.height);
        var enoughRoomBelow = viewport.bottom > (offset.bottom + dropdown.height);

        var css = {
            left: offset.left,
            top: container.bottom
        };

        // Determine what the parent element is to use for calciulating the offset
        var $offsetParent = this.$dropdownParent;

        // For statically positoned elements, we need to get the element
        // that is determining the offset
        if ($offsetParent.css('position') === 'static') {
            $offsetParent = $offsetParent.offsetParent();
        }

        var parentOffset = $offsetParent.offset();

        css.top -= parentOffset.top
        css.left -= parentOffset.left;

        var dropdownPositionOption = this.options.get('dropdownPosition');

        if (dropdownPositionOption === 'above' || dropdownPositionOption === 'below') {
            newDirection = dropdownPositionOption;
        } else {

            if (!isCurrentlyAbove && !isCurrentlyBelow) {
                newDirection = 'below';
            }

            if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
                newDirection = 'above';
            } else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
                newDirection = 'below';
            }

        }

        if (newDirection == 'above' ||
            (isCurrentlyAbove && newDirection !== 'below')) {
            css.top = container.top - parentOffset.top - dropdown.height;
        }

        if (newDirection != null) {
            this.$dropdown
                .removeClass('select2-dropdown--below select2-dropdown--above')
                .addClass('select2-dropdown--' + newDirection);
            this.$container
                .removeClass('select2-container--below select2-container--above')
                .addClass('select2-container--' + newDirection);
        }

        this.$dropdownContainer.css(css);

    };

})(window.jQuery);

function popupSelect2Init() {
    if($('.js-select-2')) {
        $('.js-select-2').select2({
            dropdownPosition: 'below'
        });
    }
}
function formSelect2Init() {
    if($('.js-select-3')) {
        $('.js-select-3').select2({
            dropdownParent: $('.form'),
            dropdownPosition: 'below'
        });
    }
}

function validateForm() {
    $('.popup__form').validate();
    $('.callback__form').validate();
    $('.form').validate();
}

function partnersPopup() {
    if($(window).width() < 1199) {
        $(document).on('click', '.partners__item', function () {
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $('.partners__item').removeClass('active');
                $(this).addClass('active');
            }
        })
    }
}

var select2Flag = true;
var select3Flag = true;
function checkInput() {
    $('.form input').on('change', function () {
        if ($(this).val() == '') {
            $(this).removeClass('not-empty');
            $(this).addClass('empty');
        } else {
            $(this).removeClass('empty');
            $(this).addClass('not-empty');
        }
    })

    $('.js-select-3').on('select2:open', function () {
        $('.select__label').addClass('not-empty');
    })
    $('.js-select-3').on('select2:select', function () {
        $('.select__label').addClass('not-empty');
        $(this).parents('.select').addClass('not-empty');
        select3Flag = false;
    })
    $(document).on('select2:close', '.js-select-3', function () {
        if (select3Flag) {
            $('.select__label').removeClass('not-empty');
            $(this).parents('.select').removeClass('not-empty');
        }

    })
    $('.js-select-2').on('select2:select', function () {
        $(this).parents('.select').addClass('not-empty');
        select2Flag = false;
    })
    $(document).on('select2:close', '.js-select-2', function () {
        if (select2Flag) {
            $(this).parents('.select').removeClass('not-empty');
        }

    })
}

function openHeaderSearch() {
    $('.js-header-search').on('click', function () {
        $('.header__search-inner').addClass('opened');
    })
}

function missClickSearch(){
    var div = $('.header__search-inner');
    var button = $('.js-header-search');
    $(document).on('click', function (event) {
        if(div.hasClass('opened') && !div.is(event.target) && div.has(event.target).length === 0 && !button.is(event.target) && button.has(event.target).length === 0) {
            div.removeClass('opened');
        }
    })
}
function listView() {
    $('.js-view-map-btn').removeClass('active');
    $('.js-view-list-btn').addClass('active');
    $('.js-view-map').removeClass('active');
    $('.js-view-list').addClass('active');
}
function mapView() {
    $('.js-view-list-btn').removeClass('active');
    $('.js-view-map-btn').addClass('active');
    $('.js-view-list').removeClass('active');
    $('.js-view-map').addClass('active');
}

function cityFilter() {
    $("#cityFilterInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $('.city__col').show();
        $('.city__item').show();
        $(".city__item").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
        $('.city__list').each(function () {
            if($(this).find('.city__item:visible').length == 0) {
                $(this).parents('.city__col').hide()
            } else {
                $(this).parents('.city__col').show()
            }
        })
    });
}

function tabs() {
    $('.js-tabs').on('click', 'a:not(.active)', function(e) {
        e.preventDefault();
        $(this)
            .addClass('active').siblings().removeClass('active')
            .parents('.tabs__wrap').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
}

function tabsSort() {
    $('.tabs__sort').on('click', function () {
        $(this).toggleClass('active');
    })
    $('.tabs__sort-option').on('click', function () {
        $('.tabs__sort-option').removeClass('active');
        $(this).addClass('active');
    })
    if($('.tabs__sort')) {
        var div = $('.tabs__sort');
        var button = $('.tabs__sort-option');
        $(document).on('click', function (event) {
            if(div.hasClass('active') && !div.is(event.target) && div.has(event.target).length === 0 && !button.is(event.target) && button.has(event.target).length === 0) {
                div.removeClass('active');
            }
        })
    }
}





function writeCookie(name, val, expires) {
    var date = new Date;
    date.setDate(date.getDate() + expires);
    document.cookie = name+"="+val+"; path=/; expires=" + date.toUTCString();
}
var test = readCookie('new-user');

function readCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function initAboutSlider() {
    if($('.privilege--slider .privilege__row')) {
        $('.privilege--slider .privilege__row').slick({
            slidesToShow: 3,
            infinite: true,
            variableWidth: true,
            arrows:false,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]
        })
    }
}

function documentScroll() {
    $('.documents__nav-item').on('click', function (e) {
        e.preventDefault();
        if($(window).width() < 1025) {
            var document = $('.documents__title').offset().top - 25;
            $('html, body').animate({scrollTop:document}, '300');
        }
    })
    $('a[href="#main"]').on('click', function (e) {
        e.preventDefault();
        $('.header__middle').removeClass('opened');
        $('.header__shadow').removeClass('active');
        var document = $('.services').offset().top - 25;
        $('html, body').animate({scrollTop:document}, '300');
    })
}

$(window).resize(function () {
    servicesSliderInit();
    typesAccord();
})

var typesFlag = false;
var triggerTypesFlag = true;


$(document).ready(function () {
    documentScroll();
    initAboutSlider();
    if($(window).width() < 768) {
        if (triggerTypesFlag) {
            typesFlag = true;
        }
        typesAccord();
    }
    if(test) {
        $('.header__app').remove();
    } else {
        writeCookie('new-user', 'true', 30);
        var headerApp =
            '        <div class="header__app">'+
            '            <div class="header__app-close">' +
            '                <svg>' +
            '                    <use xlink:href="#close-ico"></use>' +
            '                </svg>' +
            '            </div>' +
            '            <a href="#" class="header__app-row">' +
            '                <div class="header__app-ico">' +
            '                    <svg>' +
            '                        <use xlink:href="#app-ico"></use>' +
            '                    </svg>' +
            '                </div>' +
            '                <div class="header__app-desc">Приложение Просто|Банка может больше</div>' +
            '                <a href="#" class="header__app-link">' +
            '                    <span>Скачать</span>' +
            '                    <svg>' +
            '                        <use xlink:href="#arrow-right-ico"></use>' +
            '                    </svg>' +
            '                </a>' +
            '            </a>' +
            '         </div>';
        $(headerApp).insertBefore('.header__inner');
    }
    subnavOpener();
    mobmenu();
    footerMobNav();
    initMainSlider();
    initTariffsSlider();
    accordion();
    mobheaderAppClose();
    servicesSliderInit();
    termsSliderInit();
    generalTermsSliderInit();
    scrollTop();
    popupSelect2Init();
    formSelect2Init();
    validateForm();
    documentsAccordion();
    partnersPopup();
    checkInput();
    openHeaderSearch();
    missClickSearch();
    cityFilter();
    tabs();
    tabsSort();
    $(document).on('click', '.js-view-list-btn:not(.active)', function () {
        listView();
    });
    $(document).on('click', '.js-view-map-btn:not(.active)', function () {
        mapView();
    });
    $('.popup a[data-fancybox]').on('click', function () {
        $(this).parents('.popup').find('button[data-fancybox-close]').click();
    });
    var btn = $('.gototop__btn');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 150) {
            btn.addClass('active');
        } else {
            btn.removeClass('active');
        }
    });
    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
    });
    jQuery.extend(jQuery.validator.messages, {
        required: "Обязательное поле",
        remote: "Please fix this field.",
        email: "Введите корректный e-mail",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });
})
